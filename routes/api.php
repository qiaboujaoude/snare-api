<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// API endpoints for Snare Database
Route::get('/artists/{name}', 'ArtistController@getArtistName');
Route::get('/albums/{id}', 'ArtistController@getAlbum');
Route::get('/artists', 'ArtistController@getArtistList');

// endpoints for Spotify API
Route::get('/artistTracks/{artistName}', 'SpotifyAPIController@getArtistID');
Route::get('/tracks/{artist}', 'SpotifyAPIController@getArtistTopTracksIds');
Route::get('/album/{artist}/{album}', 'SpotifyAPIController@getArtistAlbumId');
Route::get('/album/{artist}', 'SpotifyAPIController@getArtistAlbums');
Route::get('/artist/{artist}', 'SpotifyAPIController@getRelatedArtists');
Route::get('/spotify/{artist}', 'SpotifyAPIController@getArtist');

// endpoints for Youtube API
Route::get('/review/{artist}/{album}', 'YoutubeController@getReview');
Route::get('/video/{artist}', 'YoutubeController@getMostViewedTracks');
Route::get('/video/{artist}/{album}', 'YoutubeController@getAlbumVideo');

//end points for Metacritic
Route::get('/metascore/{album}/{artist}', 'MetacriticController@getArtist');
Route::get('/lastfm/{artist}', 'MetacriticController@getArtistPicture');
