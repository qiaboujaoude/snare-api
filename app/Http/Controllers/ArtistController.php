<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Album;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class ArtistController extends Controller
{
    function getArtistName($name) 
    {
        $result = str_replace('+', ' ', $name);

        $artist = Artist::where('artist_name', 'like', $result)->get();

        // This works and I don't know how.
        // Don't fix whats not broken 
        for ($i=1; $i<=count($artist[0]->albums); $i++) {
            $artist[0]->albums[$i-1];
        }

        if ($artist->isEmpty()) {
            // return Response::json('error', 404);
            return response()->json('error', 404);
        } else {
            return response()->json($artist);
        }
    }

    function getAlbum($id) 
    {
        $result = Album::find($id);
        if (!$result) {
            return Response::json('error. album not found', 404);
        } else {
            return response()->json($result);
        }
    }

    function getArtistList() 
    {
        $result =  Artist::pluck('artist_profile_picture', 'artist_name');

        return response()->json($result);

    }

}





