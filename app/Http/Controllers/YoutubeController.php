<?php

namespace App\Http\Controllers;

use Youtube;
use Illuminate\Http\Request;

class YoutubeController extends Controller
{

    function getMostViewedTracks($artist) 
    {
        $params = [
            'q'          => $artist,
            'part'       => 'snippet',
            'type'       => 'video',
            'maxResults' => '5',
            'order'      => 'viewCount'
        ];

        $results = Youtube::searchAdvanced($params);

        return response()->json($results);
    }

    function getAlbumVideo($artist, $album)
    {
        $query = $artist . ' ' . $album;

        $params = [
            'q'             => $query . '|' . $query . ' full album',
            'part'          => 'snippet',
            'type'          => 'video',
            'maxResults'    => '3',
            'order'         => 'viewCount',
            'videoDuration' => 'long'
        ];

        $results = Youtube::searchAdvanced($params);
        
        $videoTitle = $results[0]->snippet->title;

        $validateArtist = stristr($videoTitle, $artist);

        $validateAlbum  = stristr($videoTitle, $album);

        if((!$validateArtist) || (!$validateAlbum)) {
            return response()->json(['error' => 'Album not found'], 404);
        } else {
            return response()->json($results);        
        }
    }

    function getReview($artist, $album)
    {
        $query = $artist . ' ' . $album;

        $videoList = Youtube::searchChannelVideos($query, 'UCt7fwAhXDy3oNFTAzF2o8Pw', 1);

        $videoTitle = $videoList[0]->snippet->title;

        $validate = stristr($videoTitle, $album);

        if(!$validate) {
            return response()->json(['error' => 'Review not found'], 404);
        } else {
            return response()->json($videoList[0]->id->videoId);
        }
    }
}
