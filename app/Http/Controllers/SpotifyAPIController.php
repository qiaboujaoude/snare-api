<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SpotifyWebAPI;

class SpotifyAPIController extends Controller
{

    private $spotify;
    public  $artistId;
    public  $artistName;

    public  function __construct(Request $request) 
    {
        $this->setToken();

        $this->artistName = $request->route()->parameter('artist');

        $this->artistId = $this->getArtistId();
    }
    
    private function setToken() 
    {
        $session = new SpotifyWebAPI\Session(
            '5d1de969350d41c2a2c36ff9dd9c02db',
            '5316051397d940298af3274e3b563772'
        );
        
        $this->spotify = new SpotifyWebAPI\SpotifyWebAPI();
        
        $session->requestCredentialsToken();

        $accessToken = $session->getAccessToken();
        
        $this->spotify->setAccessToken($accessToken);
    }

    private function getArtistId() 
    {
        $result = $this->spotify->search($this->artistName, 'artist');

        return $result->artists->items[0]->id;
    }

    function getArtistTopTracksIds() 
    {
        $options = [
            'country' => 'US'
        ];

        $result = $this->spotify->getArtistTopTracks($this->artistId, $options);

        return response()->json($result);
    }

    function getArtistAlbumId(Request $request)
    {
        $album_name = $request->route()->parameter('album');

        $query = $album_name . ', ' . $this->artistName;

        $result = $this->spotify->search($query, 'album');

        return response()->json($result);
    }

    function getArtistAlbums() 
    {
        $result = $this->spotify->getArtistAlbums($this->artistId);
        return response()->json($result);
    }

    function getRelatedArtists() 
    {
        $result = $this->spotify->getArtistRelatedArtists($this->artistId);
        return response()->json($result);
    }

    function getArtist() 
    {
        $result = $this->spotify->getArtist($this->artistId);

        return response()->json($result);
    }
}